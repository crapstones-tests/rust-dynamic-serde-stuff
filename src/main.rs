use serde::{Deserialize, Serialize};
use serde_json::json;

/// "Outer" struct that is just here for demonstration purposes.
#[derive(Debug, Deserialize, Serialize)]
struct Outer {
    internal: String,
    data: Wrapper,
}

/// The outer Struct implements a few methods that call internal methods of the wrapped structs.
impl Outer {
    /// A function that creates a `String` from some internal state and the output of a trait-object function.
    pub fn format(&self) -> String {
        format!("[{}, {}]", self.internal, self.data.display())
    }

    /// Changes the internal value and calls a function that modifies the trait-object via a mutable reference.
    pub fn do_something(&mut self) {
        self.data.modify();
        self.internal = "something other".to_string();
    }
}

/// The `trait` that should be implemented.
trait SomeTrait {
    /// Returns some internal state.
    fn display(&self) -> String;
    /// Modifies the internal state.
    fn modify(&mut self);
}

/// The wrapper `enum` that dispatches the calls to the right trait-object-implementation without a v-table.
#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
enum Wrapper {
    One(NameData),
    Two(IdData),
}

impl Wrapper {
    /// Helper function that dispatches with a immutable reference.
    #[inline(always)]
    fn inner(&self) -> &dyn SomeTrait {
        match self {
            Wrapper::One(inner) => inner,
            Wrapper::Two(inner) => inner,
        }
    }

    /// Helper function that dispatches with a mutable reference.
    #[inline(always)]
    fn inner_mut(&mut self) -> &mut dyn SomeTrait {
        match self {
            Wrapper::One(inner) => inner,
            Wrapper::Two(inner) => inner,
        }
    }

    /// The first dispatch function. Can also be implemented via the `trait impl`.
    #[inline(always)]
    pub fn display(&self) -> String {
        self.inner().display()
    }

    /// The other dispatch function. Can also be implemented via the `trait impl`.
    #[inline(always)]
    pub fn modify(&mut self) {
        self.inner_mut().modify();
    }
}

/// The first struct that should `impl` the `trait`.
#[derive(Debug, Deserialize, Serialize)]
struct NameData {
    name: String,
}

/// The second struct that should `impl` the `trait`.
#[derive(Debug, Deserialize, Serialize)]
struct IdData {
    id: u32,
}

/// The trait `impl`s for the first struct
impl SomeTrait for NameData {
    fn display(&self) -> String {
        self.name.clone()
    }

    fn modify(&mut self) {
        self.name = "Fritz".to_string();
    }
}

/// The trait `impl`s for the second struct
impl SomeTrait for IdData {
    fn display(&self) -> String {
        self.id.to_string()
    }

    fn modify(&mut self) {
        self.id = 21;
    }
}

fn main() {
    let input = json!(
    [
        {
            "internal": "something",
            "data": {"one": {"name": "Hans"}}
        },
        {
            "internal": "some different value",
            "data": {"two": {"id": 42}}
        }
        ]
    );

    // deserialize everything into different types that behaves the same
    let data_vector: Vec<Outer> = serde_json::from_value(input).unwrap();

    for mut item in data_vector.into_iter() {
        println!("before: {}", item.format());
        println!("{}", serde_json::to_string_pretty(&item).unwrap());

        // call the same function but get different results
        item.do_something();

        println!("after: {}", item.format());
        println!("{}", serde_json::to_string_pretty(&item).unwrap());

        println!();
    }
}
