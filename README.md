# Rust Dynamic Serde Stuff

Imagine you want to have a bunch of structs that implement a certain `trait` but should also be de-/serializable.
You can just wrap them in an `enum` and call the methods from this `enum` after you cast them to trait-objects.

See the [main](src/main.rs) with many comments for more information.
